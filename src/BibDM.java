import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      Integer res = null;
      for (Integer elem : liste) {
        if (res == null || elem < res){
          res = elem;
        }
        //Remplace la valeur minimal si présente
      }
        return res;
    }



    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
      for (T elem : liste) {
        if (valeur.compareTo(elem) >= 0) {
          //Renvoie false si un element est inférieur à valeur
          return false;
        }
      }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> listeR = new ArrayList<>(); //Création de la liste Résultante
        for (T elemL1 : liste1) {
          //Parcours de la premiere liste
          for (T elemL2 : liste2) {
            //Parcours de la seconde liste
            if (elemL1.compareTo(elemL2) == 0 && !listeR.contains(elemL2)) { listeR.add(elemL1); }
              //Ajout de l'élément dans la liste résultante SI il est commun aux deux listes
            if (elemL2.compareTo(elemL1) > 0) { break; }
              //Arrête la boucle liste2 si la valeurs (elemL2) suivante est forcement supérieur à elemL1
          }
          if (elemL1.compareTo(liste2.get(liste2.size()-1)) > 0) { break; }
            //Arrête la boucle liste1 si la valeurs (elemL1) suivante est forcement supérieur à toute valeurs de elemL2
        }
        return listeR;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
      List<String> words = new ArrayList<>();
      String word = "";
      for (char c : texte.toCharArray()) {
        //Pour chaque caractere de texte
        if (c == ' ' && !word.equals("")) {
          words.add(word);
          word = "";
        }
          //Ajoute word dans la liste de mot lorsque le caractere parcouru est un espace
        else if (c != ' ') { word += c; }
          //Incrémente le mot avec le caractere suivant
      }
      if (word != "") { words.add(word); }
        //Utile à la sortie de boucle signifiant la fin d'un mot
      return words;
    }

    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){

      Map<String, Integer> words = new HashMap<>();
      String word = "";
      int occMax = 0;
      String wordMax = null;
      for (char c : texte.toCharArray()) {
        //Pour chaque caractere de texte
        if (c == ' ' && !word.equals("")) {
          //Ajoute word dans la liste de mot lorsque le caractere parcouru est un espace
          if (!words.containsKey(word)) { words.put(word, 0); }
            //Crée un element avec la clé correspondant à word si cet élément n'existe pas
          words.put(word, words.get(word)+1);
          //Incrémente la valeur associé à la clé word
          if (words.get(word) >= occMax) {
            if (words.get(word) > occMax) {
              occMax = words.get(word);
              wordMax = word;
            }
            else if (word.compareTo(wordMax) < 0) { wordMax = word; }
          }
          //Actualise le mot majoritaire en fonction de la derniere modification
          word = "";
        }
        else if (c != ' ') { word += c; }
          //Incrémente le mot avec le caractere suivant
      }
      if (word != "") {
        if (!words.containsKey(word)) { words.put(word, 0); }
          //Crée un element avec la clé correspondant à word si cet élément n'existe pas
        words.put(word, words.get(word)+1);
        //Incrémente la valeur associé à la clé word
        if (words.get(word) >= occMax) {
          if (words.get(word) > occMax) {
            occMax = words.get(word);
            wordMax = word;
          }
            //Occurence supérieur
          else if (word.compareTo(wordMax) < 0) { wordMax = word; }
            //Ordre alphabetique
        }
          //Actualise le mot majoritaire en fonction de la derniere modification
      }
        //Utile à la sortie de boucle signifiant la fin d'un mot
      return wordMax;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      int nbOpens = 0;
      int nbClosed = 0;
      int correctlyOpened = 0;
      for (char c : chaine.toCharArray()) {
        if (c == '(') {
          nbOpens++;
          correctlyOpened++;
        }
          //Incrémente le nombre de parenthese ouvrante si la parenthese est ouvrante et Incrémente le nombre de parenthese fermante possible
        else if (c == ')') {
          nbClosed++;
          correctlyOpened--;
        }
          //Incrémente le nombre de parenthese fermante si la parenthese est fermante
        if (correctlyOpened < 0) { return false; }
          //Retourne false si le nombre de parenthese fermante est supérieur a celui des parenthese ouvrante
      }
        return nbOpens == nbClosed;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      int nbParOpens = 0;
      int nbParClosed = 0;
      int nbBraOpens = 0;
      int nbBraClosed = 0;
      int correctlyParOpened = 0;
      int correctlyBraOpened = 0;
      boolean isPar = false;
      char prec = 'a';
      for (char c : chaine.toCharArray()) {
          if (c == '(') {
            nbParOpens++;
            correctlyParOpened++;
            isPar = true;
          }
            //Incrémente le nombre de parenthese ouvrante si la parenthese est ouvrante et Incrémente le nombre de parenthese fermante possible
          else if (c == ')') {
            nbParClosed++;
            correctlyParOpened--;
            if (!(isPar || prec == ']')) { return false; }
              //Retourne false si le caractere precedent est un crochet ouvrant
            isPar = true;
          }
            //Décrémente le nombre de parenthese fermante si la parenthese est fermante
          else if (c == '[') {
            nbBraOpens++;
            correctlyBraOpened++;
            isPar = false;
          }
            //Incrémente le nombre de crochet ouvrante si la crochet est ouvrante et Incrémente le nombre de crochet fermante possible
          else if (c == ']') {
            nbBraClosed++;
            correctlyBraOpened--;
            if (!(!isPar || prec == ')')) { return false; }
            //Retourne false si le caractere precedent est une parenthèse ouvrante
            isPar = false;
          }
          //Décrémente le nombre de crochet fermante si la crochet est fermante
          if (correctlyBraOpened < 0) { return false; }
            //Retourne false si le nombre de crochet fermante est supérieur a celui des crochet ouvrante
          if (correctlyParOpened < 0) { return false; }
            //Retourne false si le nombre de parenthese fermante est supérieur a celui des parenthese ouvrante
          prec = c;
          //Actualise le caractere precedent pour le tours suivant
      }
        return (nbParOpens == nbParClosed && nbBraOpens == nbBraClosed);
    }



    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      int center;
      int end = liste.size();
      int start = 0;
      boolean found = false;

      while (start < end && !found) {
          center = ((start + end) / 2);
          //La valeur du center est initialisé à chaque tour de boucle
          if (valeur.compareTo(liste.get(center)) == 0) { found = true; }
            //Si la valeur au center est égale à valeur, on l'a trouvé
          else if (valeur.compareTo(liste.get(center)) > 0) { start = center + 1; }
            //Si la valeur au center est inférieur à valeur, le début du parcours se fait au centre
          else { end = center; }
            //Sinon la fin du parcours se fait au centre
      }
        //Tant que l'on à pas parcouru l'ensemble de la liste ou trouvé valeur
      return found;

    }




}
